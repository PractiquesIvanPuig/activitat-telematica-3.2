package ipuig.m06.telematic;

public class Main {

    public static void menu() {

        int opcio;

        do {

            System.out.println( "0 - Sortir" +
                    "\n1 - Llegir d'un fitxer XML sequencialment" +
                    // Es demanara nom del fitxer

                    "\n2 - Llegir d'un fitxer XML pel metode sintactic" +
                    // Es demanara el nom del fitxer

                    "\n3 - Mostar per pantalla totes les assignatures amb les seves dades (numero, nom, durada i la llista d'alumnes)" +

                    "\n4 - Afegir una assignatura" + 
                    // Es demanaran les dades basiques (numero, nom i durada)

                    "\n5 - Afegir un alumne a una assignatura" +
                    // Es demanara el numero de l'assignatura (si existeix)
                    // Es demanaran les dades de l'alumne (nom, dni, si es repetidor)
                    // S'afegira a la llista d'alumnes de l'assignatura

                    "\n6 - Guardar a disc en XML amb les assignatures");

            opcio = Util.getUserInt();

            switch(opcio) {

                case 0: 
                    System.out.println("--------- Sortint ---------");
                    break;

                case 1:
                    System.out.println("------ Llegir fitxer sequencialment ------");
                    Assignatura.eliminarAssignatures(); // Eliminem les dades que hi havien abans de carregar el nou xml perque no es solapin
                    Assignatura.llegirSequencialment();
                    break;

                case 2:
                    System.out.println("------ Llegir fitxer sintacticament ------");
                    Assignatura.eliminarAssignatures();
                    Assignatura.llegirSintacticament();
                    break;

                case 3:
                    System.out.println("------ Mostrar per pantalla totes les assignatures amb les seves dades ------");
                    Assignatura.imprimirTotes();
                    break;

                case 4:
                    System.out.println("------ Afegir una assignatura ------");
                    Assignatura.afegirAssignatura();
                    break;

                case 5:
                    System.out.println("------ Afegir un alumne a una assignatura ------");
                    if(Assignatura.getLlistaAssignatures().isEmpty()) {
                        System.out.println("Encara no hi ha assignatures creades");
                    }

                    else {
                        Assignatura.triaAssignatura().afegirAlumne();
                    }

                    break;

                case 6:
                    System.out.println("------ Guardar a disc en XML amb les assignatures ------");
                    Assignatura.guardarXml("assignatures.xml");
                    break;

                default:
                    break;
            }
        }

        while(opcio != 0);
    }

    public static void main(String[] args) {
        menu();
    }
}
