package ipuig.m06.telematic;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Alumne {
    private String nom;
    private String DNI;  // numero + lletra
    private boolean esRepetidor; // true si ha repetit
    private int assignatura;

    public Alumne(String nom, String DNI, boolean esRepetidor) {
        this.nom = nom;
        this.DNI = DNI;
        this.esRepetidor = esRepetidor;
    }

    public Alumne(String nom, String DNI, boolean esRepetidor, int assignatura) {
        this.nom = nom;
        this.DNI = DNI;
        this.esRepetidor = esRepetidor;
        this.assignatura = assignatura;
    }

    public int getAssignatura() {
        return this.assignatura;
    }

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDNI() {
		return this.DNI;
	}

	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	public boolean getRepetidor() {
		return this.esRepetidor;
	}

	public void setRepetidor(boolean esRepetidor) {
		this.esRepetidor = esRepetidor;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((DNI == null) ? 0 : DNI.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumne other = (Alumne) obj;
		if (DNI == null) {
			if (other.DNI != null)
				return false;
		} else if (!DNI.equals(other.DNI))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Alumne amb DNI: " + this.DNI + ", Nom: " + this.nom + (this.esRepetidor ? ", ha repetit curs" : ", no es repetidor");
	}

    public void imprimir() {
        System.out.println("\t- " + this);
    }

    public void guardarXml(Element nodeAlumnes, Document doc) {
        Element nodeAlumne = doc.createElement("alumne");
        nodeAlumnes.appendChild(nodeAlumne);

        // Nom alumne
        Element nodeNom = doc.createElement("nom");
        nodeNom.setTextContent(this.getNom());
        nodeAlumne.appendChild(nodeNom);

        // DNI alumne
        Element nodeDNI = doc.createElement("dni");
        nodeDNI.setTextContent(this.getDNI());
        nodeAlumne.appendChild(nodeDNI);

        // repetidor? alumne
        Element nodeRepetidor = doc.createElement("repetidor");
        nodeRepetidor.setTextContent((this.getRepetidor() ? "Si" : "No"));
        nodeAlumne.appendChild(nodeRepetidor);
    }
}
