package ipuig.m06.telematic;


import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Assignatura {
    private String nom;
    private int numero;
    private int durada; 
    private ArrayList<Alumne> alumnes = new ArrayList<>();
    private static ArrayList<Assignatura> assignatures = new ArrayList<>();

    public Assignatura(String nom, int numero, int durada) {
        this.nom = nom;
        this.numero = numero;
        this.durada = durada;
        assignatures.add(this);
    }

    public static ArrayList<Assignatura> getLlistaAssignatures() {
        return assignatures;
    }

    public static void eliminarAssignatures() {
        assignatures = new ArrayList<>();
    }

    public static void afegirAssignatura() {
        System.out.println("Introdueix el nom de l'assignatura");
        String nom = Util.getUserString();

        System.out.println("Introdueix el numero de l'assignatura");
        int numero = Util.getUserInt();

        System.out.println("Introdueix la durada");
        int durada = Util.getUserInt();

        new Assignatura(nom, numero, durada);
    }

    public ArrayList<Alumne> getLlistaAlumnes() {
        return this.alumnes;
    }

    public void afegirAlumne() {

        System.out.println("Introdueix el nom del alumne");
        String nom = Util.getUserString();

        System.out.println("Introdueix el DNI del alumne");
        String DNI = Util.getUserString();

        System.out.println("Es repetidor? si / no");
        boolean esRepetidor = Util.getUserBoolean();

        this.getLlistaAlumnes()
            .add(new Alumne(nom, DNI, esRepetidor));
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }


    public int getDurada() {
        return durada;
    }

    public void setDurada(int durada) {
        this.durada = durada;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + durada;
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + numero;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Assignatura other = (Assignatura) obj;
        if (durada != other.durada)
            return false;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (numero != other.numero)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Assignatura no - " + this.numero +
            ":\nnom: " + this.nom +
            "\ndurada: " + this.durada;
    }

    public void imprimir() {
        System.out.println(this);

        if(this.getLlistaAlumnes().isEmpty()) {
            System.out.println("Encara no hi ha alumnes registrats");
        }

        else {
            System.out.println("Hi participen els alumnes seguents: ");
            this.getLlistaAlumnes()
                .forEach(Alumne::imprimir);
        }
    }

    public static void imprimirTotes() {
        if(assignatures.isEmpty()) {
            System.out.println("Encara no hi ha assignatures creades");
        }

        else {
            assignatures
                .forEach(assignatura -> {
                    System.out.println("_____________");
                    assignatura.imprimir();
                    System.out.println();
                });
        }
    }

    public static Assignatura triaAssignatura() {
        System.out.println("A quina assignatura li vols afegir l'alumne?");
        assignatures
            .forEach(element -> {
                System.out.println(assignatures.indexOf(element) + " - " + element.getNom());
            });

        return assignatures.get(Util.getUserInt());
    }

    public static void llegirSequencialment() {

        System.out.println("Introdueix el nom del arxiu \n(el programa guarda per defecte amb el nom de assignatures.xml)\ni tambe esta disponible el fitxer de proves \"test2.xml\"");
        String path = Util.getUserString();

        try {
            Document doc = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(path);

            Node root = doc.getDocumentElement();
            NodeList subjectList = root.getChildNodes(); // Obtenim llistat d'assignatures
            NodeList subject;
            NodeList studentList;
            NodeList student;
            int posicio = 0;

            // Recrear les assignatures
            String subjectName = "";
            int subjectNumber = 0;
            int subjectDuration = 0;

            // Recrear els alumnes
            String studentName = "";
            String studentDni = "";
            boolean studentF = false;

            ArrayList<Alumne> alumnat = new ArrayList<>();

            for(int i = 1; i < subjectList.getLength(); i+=2) { // Assignatures
                subject = subjectList.item(i).getChildNodes();
                posicio++;

                for(int j = 1; j < subject.getLength(); j+=2) {

                    if(subject.item(j).getNodeName().equals("nom")) {
                        subjectName = subject.item(j).getTextContent();
                    }

                    else if(subject.item(j).getNodeName().equals("numero")) {
                        subjectNumber = Integer.parseInt(subject.item(j).getTextContent());
                    }

                    else if(subject.item(j).getNodeName().equals("durada")) {
                        subjectDuration = Integer.parseInt(subject.item(j).getTextContent());
                    }

                    else if(subject.item(j).getNodeName().equals("alumnes")) { 
                        studentList = subject.item(j).getChildNodes(); // Array alumnes

                        for(int k = 1; k < studentList.getLength(); k+=2) {
                            student = studentList.item(k).getChildNodes();

                            for(int e = 1; e < student.getLength(); e+=2) {

                                if(student.item(e).getNodeName().equals("nom")) {
                                    studentName = student.item(e).getTextContent();
                                }

                                else if(student.item(e).getNodeName().equals("dni")) {
                                    studentDni = student.item(e).getTextContent();
                                }

                                else if(student.item(e).getNodeName().equals("repetidor")) {
                                    studentF = student.item(e).getTextContent().matches("[sS]i");
                                }
                            }

                            // Alumnes
                            alumnat.add(new Alumne(studentName, studentDni, studentF, posicio));
                        }
                    }
                } 

                // Assignatures
                new Assignatura(subjectName, subjectNumber, subjectDuration);
            }

            // Afegeixo cada alumne a la seva assignatura
            alumnat.forEach(alumne -> {
                assignatures.get(alumne.getAssignatura()).getLlistaAlumnes().add(alumne);
            });

            System.out.println("\nDADES CARREGADES AMB EXIT!\n");
        }

        catch(FileNotFoundException e) {
            System.out.println("No hi ha cap fitxer amb aquest nom o ruta comprova el path, o be crea un de nou");
        }

        catch(IOException e) {
            e.printStackTrace();
        }

        catch(SAXException e) {
            e.printStackTrace();
        }

        catch(ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static void llegirSintacticament() {

        System.out.println("Introdueix el nom del arxiu \n(el programa guarda per defecte amb el nom de assignatures.xml)\ni tambe esta disponible el fitxer de proves \"test2.xml\"");
        String path = Util.getUserString();

        try {
            Document doc = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(path);

            // assignatures
            XPathExpression expr = XPathFactory.newInstance().newXPath().compile("/assignatures/assignatura");
            NodeList subjectList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            NodeList subject;
            String subjectName = "";
            int subjectNumber = 0;
            int subjectDuration = 0;

            // alumnes
            NodeList studentList;
            NodeList student;
            String studentName = "";
            String studentDni = "";
            boolean studentF = false;
            ArrayList<Alumne> alumnat = new ArrayList<>();

            System.out.println(subjectList.getLength());
            for(int i = 0; i < subjectList.getLength(); i++) {
                subject = subjectList.item(i).getChildNodes();

                for(int j = 1; j < subject.getLength(); j+=2) {

                    if(subject.item(j).getNodeName().equals("nom")) {
                        subjectName = subject.item(j).getTextContent();
                    }

                    else if(subject.item(j).getNodeName().equals("numero")) {
                        subjectNumber = Integer.parseInt(subject.item(j).getTextContent());
                    }

                    else if(subject.item(j).getNodeName().equals("durada")) {
                        subjectDuration = Integer.parseInt(subject.item(j).getTextContent());
                    }

                    else if(subject.item(j).getNodeName().equals("alumnes")) { 
                        studentList = subject.item(j).getChildNodes(); // Array alumnes

                        for(int k = 1; k < studentList.getLength(); k+=2) {
                            student = studentList.item(k).getChildNodes();

                            for(int e = 1; e < student.getLength(); e+=2) {

                                if(student.item(e).getNodeName().equals("nom")) {
                                    studentName = student.item(e).getTextContent();
                                }

                                else if(student.item(e).getNodeName().equals("dni")) {
                                    studentDni = student.item(e).getTextContent();
                                }

                                else if(student.item(e).getNodeName().equals("repetidor")) {
                                    studentF = student.item(e).getTextContent().matches("[sS]i");
                                }
                            }

                            // Alumnes
                            alumnat.add(new Alumne(studentName, studentDni, studentF, i));
                        }
                    }
                }

                new Assignatura(subjectName, subjectNumber, subjectDuration);
            }

            alumnat.forEach(alumne -> {
                assignatures.get(alumne.getAssignatura()).getLlistaAlumnes().add(alumne);
            });

            System.out.println("\nDADES CARREGADES AMB EXIT!\n");
        }

        catch(FileNotFoundException e) {
            System.out.println("No hi ha cap fitxer amb aquest nom o ruta comprova el path, o be crea un de nou");
        }

        catch(IOException e) {
            e.printStackTrace();
        }

        catch(SAXException e) {
            e.printStackTrace();
        }

        catch(ParserConfigurationException e) {
            e.printStackTrace();
        }

        catch(XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    public static void guardarXml(String path) {
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            // Assignatures
            Element nodeAssignatures = doc.createElement("assignatures");
            doc.appendChild(nodeAssignatures);

            assignatures
                .forEach(assignatura -> {

                    // Assignatura
                    Element nodeAssignatura = doc.createElement("assignatura");
                    nodeAssignatures.appendChild(nodeAssignatura);

                    // Nom de la assignatura
                    Element nodeNom = doc.createElement("nom");
                    nodeNom.setTextContent(assignatura.getNom());
                    nodeAssignatura.appendChild(nodeNom);

                    // Numero de la assignatura
                    Element nodeNumero = doc.createElement("numero");
                    nodeNumero.setTextContent(Integer.toString(assignatura.getNumero()));
                    nodeAssignatura.appendChild(nodeNumero);

                    // Durada de la assignatura
                    Element nodeDurada = doc.createElement("durada");
                    nodeDurada.setTextContent(Integer.toString(assignatura.getDurada()));
                    nodeAssignatura.appendChild(nodeDurada);

                    // Alumnes
                    Element nodeAlumnes = doc.createElement("alumnes");
                    nodeAssignatura.appendChild(nodeAlumnes);

                    assignatura
                        .getLlistaAlumnes()
                        .forEach(alumne -> {
                            alumne.guardarXml(nodeAlumnes, doc);
                        });
                });

            try {
                TransformerFactory.newInstance().newTransformer()
                    .transform(new DOMSource(doc), new StreamResult(new File (path)));
            }

            catch(TransformerException e) {
                e.printStackTrace();
            }
        }

        catch(ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
