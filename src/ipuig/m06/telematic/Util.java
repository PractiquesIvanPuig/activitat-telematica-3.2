package ipuig.m06.telematic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Util {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static int getUserInt() {

        try {
            return Integer.parseInt(in.readLine());
        }

        catch(NumberFormatException e) {
            System.out.println("Introdueix un enter");
            return getUserInt();
        }

        catch(IOException e) {
            System.out.println("Introdueix un enter");
            return Integer.parseInt(getUserString());
        }
    }

    public static String getUserString() {

        try {
            return in.readLine();
        }

        catch(IOException e) {
            System.out.println("Introdueix caracters valids!");
            return getUserString();
        }
    }

    public static boolean getUserBoolean() {

        try {
            return in.readLine().matches("[yY]es|[sS]i|[tT]rue|[rR]epetidor");
        }

        catch(IOException e) {
            System.out.println("Introdueix caracters valids!");
            return getUserBoolean();
        }
    }
}
